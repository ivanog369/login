// Animaciones de las transiciones de los links
function transitions() {
	$('#login-form-link').click(function (e) {
		$("#login-form").delay(100).fadeIn(100);
		$("#register").fadeOut(100);
		$("#forgot").fadeOut(100);
		$('#register-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
	$('#register-form-link').click(function (e) {
		$("#register").delay(100).fadeIn(100);
		$("#login-form").fadeOut(100);
		$("#forgot").fadeOut(100);
		$('#login-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
	$('#forgot-password-link').click(function (e) {
		$("#forgot").delay(100).fadeIn(100);
		$("#login-form").fadeOut(100);
		$('#login-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
}

// Oculta los mensajes de error
function clearMsg() {
	$('#fields-empty').css('display', 'none');
	$('#fields-error').css('display', 'none');
	$('#fields-error-1').css('display', 'none');
	$('#fields-error-2').css('display', 'none');
	$('#fields-error-3').css('display', 'none');
	$('#fields-error-4').css('display', 'none');
	$('#fields-error-5').css('display', 'none');
	$('#fields-error-6').css('display', 'none');
}

// Borde rojo en los input al superar la longitud maxima del string
function borderAlert(DOMElem) {
	$(DOMElem).keyup(function () {
		const minLen = 8;
		const maxLen = 16;
		var $lenPass = $(this).val().length;
		if (($lenPass >= 0 && $lenPass < minLen) || $lenPass > maxLen)
			$(this).css('border', '1px solid red');
		else
			$(this).css('border', '');
	});
}