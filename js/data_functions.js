$.getScript('js/aniApp.js');
// Constante global para validar el email
const expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
// Cuando el documento este listo, ejecuta la funcion (la carga de todo el login)
$(document).ready(function () {
	// Carga los archivos html
	$('.container').load('html/login.html', function () {
		transitions();
		borderAlert("input[type='password']");

		$('#login-form-link').click(function () {
			clearMsg();
		});

		$('#register-form-link').click(function () {
			clearMsg();
			$('#register').load('html/register.html', function () {
				borderAlert("input[type='password']");

				// Registra un usuario
				$('#register-submit').click(function (e) {
					e.preventDefault();
					clearMsg();

					var $user = $("#rusername").val();
					var $email = $("#remail").val();
					var $passw = $("#rpassword").val();
					var $cpass = $("#rconfirm_password").val();
					var reqlen = $user.length * $email.length * $passw.length * $cpass.length;
					// Verifica los campos (si estan vacios, email valido, contraseñas iguales y su longitud)
					if (!reqlen)
						$('#fields-empty').fadeIn();
					else if (!expr.test($email))
						$('#fields-error-6').fadeIn();
					else if ($passw != $cpass)
						$('#fields-error-2').fadeIn();
					else if ($passw.length < 8 || $passw.length > 16)
						$('#fields-error-5').fadeIn();
					else {
						// Encripta la contraseña introducida con la funcion sha1()
						$passw = sha1($passw);
						// Envia los datos al servidor por ajax metodo post
						$.post('php/register.php', { rusername: $user, remail: $email, rpassword: $passw, rconfirm_password: $cpass }, function (cb) {
							if (cb.status === "Success")
								$('#panel').html(cb.data);
							else
								$('#fields-error-3').fadeIn();
						}, "json");
					}
					// Limpia los campos de las contraseñas
					$("input[type='password']").val('');
				});
			});
		});

		$('#forgot-password-link').click(function () {
			clearMsg();
			$('#forgot').load('html/forgotPass.html', function () {
				// Luego de validar el campo muestra una contraseña alternativa para la recuperacion de la cuenta
				$('#forgot-password-submit').click(function (e) {
					e.preventDefault();
					clearMsg();
					var $email = $("#femail").val();

					if (!$email.length)
						$('#fields-empty').fadeIn();
					else if (!expr.test($email))
						$('#fields-error-6').fadeIn();
					else {
						$.post('php/forgot.php', { femail: $email }, function (cb) {
							if (cb.status === "Success")
								$('#panel').html(cb.data);
							else
								$('#fields-error-4').fadeIn();
						}, "json");
					}
				});
			});
		});

		// Enviar los datos del login al servidor luego de pasar el test de validacion
		$("#login-submit").click(function (e) {
			e.preventDefault();
			clearMsg();

			var $email = $('#email').val();
			var $passw = $('#password').val();
			var reqlen = $email.length * $passw.length;

			if (!reqlen)
				$('#fields-empty').fadeIn();
			else if (!expr.test($email))
				$('#fields-error-6').fadeIn();
			else {
				$.post('php/recaptcha.php', $('#login-form').serialize(), function (cb) {
					if (cb) {
						$passw = sha1($passw);
						$.ajax({
							cache: false,
							type: "post",
							dataType: "json",
							url: "php/login.php",
							data: { email: $email, password: $passw },
							success: function (data) {
								if (data.status === "Success")
									$('#panel').html(data.data);
								else {
									$('#fields-error').fadeIn();
									grecaptcha.reset();
								}
							}
						});
					}
					else
						$('#fields-error-1').fadeIn();
				});
			}
			$("input[type='password']").val('');
		});
	});
});